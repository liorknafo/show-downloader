FROM alpine as with-node

RUN apk add --no-cache \
    chromium \
    nss \
    freetype \
    harfbuzz \
    ca-certificates \
    ttf-freefont \
    nodejs \
    yarn

WORKDIR /app

# Install puppeteer so it's available in the container.
RUN addgroup -g 1000 chrome && adduser -u 1000 -G chrome -h /home/chrome -D chrome\
    && mkdir -p /home/chrome/Downloads /app \
    && chown -R chrome:chrome /home/chrome \
    && chown -R chrome:chrome /app

# Run everything after as non-privileged user.
USER chrome

FROM with-node as base

USER chrome

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
    PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser

COPY --chown=chrome package.json yarn.lock ./

FROM base as compiler
RUN yarn install
COPY --chown=chrome ./ ./
RUN yarn build

FROM base as prod
ENV NODE_ENV=production
RUN yarn install
USER root
RUN mkdir -p /config /animeSub /anime && \
    #chown chrome:chrome /config && \
    chown chrome:chrome /animeSub && \
    chown chrome:chrome /anime
USER chrome
COPY --chown=chrome --from=compiler /app/dist/ /app/dist/
ENTRYPOINT ["yarn", "start:prod"]

