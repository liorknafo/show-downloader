module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: ['./tsconfig.json'],
  },
  plugins: ['@typescript-eslint', 'prettier'],
  extends: ['airbnb-base', 'airbnb-typescript/base', 'prettier'],
  rules: {
    'import/prefer-default-export': 'off',
    'no-restricted-syntax': 'off',
    'no-continue': 'off',
    'no-console': 'off',
  },
};
