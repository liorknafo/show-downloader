import { rename, unlink, copyFile, mkdtemp, rm } from 'fs/promises';
import { watch } from 'fs';
import { tmpdir } from 'os';
import path from 'path';

export async function waitForFileChange(dir: string, timeout: number): Promise<void> {
  const watcher = watch(dir);
  let timeoutItem: NodeJS.Timeout | undefined;

  await new Promise<void>((res) => {
    watcher.on('change', (eventType) => {
      if (eventType === 'rename') res();
    });
    timeoutItem = setTimeout(res, timeout);
  });

  watcher.close();
  if (timeoutItem !== undefined) clearTimeout(timeoutItem);
}

export async function useTempDir<T>(tmpDirNameStart: string, task: (dir: string) => Promise<T> | T): Promise<T> {
  const dir = await mkdtemp(path.join(tmpdir(), tmpDirNameStart));
  try {
    return await task(dir);
  } finally {
    setTimeout(() => rm(dir, { recursive: true }), 60000);
  }
}

export async function move(oldPath: string, newPath: string): Promise<void> {
  try {
    await rename(oldPath, newPath);
  } catch (err: any) {
    if (err?.code === 'EXDEV') {
      await copyFile(oldPath, newPath);
      await unlink(oldPath);
    } else {
      throw err;
    }
  }
}
