export interface ShowSubscription {
  basePath: string;
  showName: string;
  seasonNumber: number;
  url: string;
  siteName: string;
  offset?: number;
}
