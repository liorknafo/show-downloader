export class DownloadFail extends Error {
  code = 'DownloadFail';

  constructor() {
    super('Failed to download the ep');

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, DownloadFail.prototype);
  }
}
