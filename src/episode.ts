import path from 'path';
import type { ISite } from './iSite';
import type { Season } from './season';

// export interface Episode {
//   episodeSite: EpisodeSite;
//   site: ISite;
//   number: number;
//   season: Season;
// }
export class Episode {
  constructor(
    readonly season: Season,
    readonly episodeNumber: number,
    readonly site: ISite,
    readonly siteEpisodeData: any
  ) {}

  public get episodeName(): string {
    const season = this.season.seasonNumber.toLocaleString(undefined, {
      minimumIntegerDigits: 2,
    });
    const episode = this.episodeNumber.toLocaleString(undefined, {
      minimumIntegerDigits: 2,
    });
    return `S${season}E${episode}`;
  }

  public get episodePath(): string {
    return path.join(this.season.seasonPath, `${this.season.show.name} - ${this.episodeName}`);
  }
}
