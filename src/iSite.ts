import type { Episode } from './episode';
import type { Season } from './season';

export interface ISite {
  getEpisodesList(season: Season, url: string): Promise<Episode[]>;
  downloadEpisode(episode: Episode): Promise<void>;
  readonly name: string;
}
