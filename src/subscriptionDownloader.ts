import _ from 'lodash';
import AsyncLock from 'async-lock';
import { injectAll, singleton } from 'tsyringe';
import { ShowSubscription } from './showSubscription';
import { Show } from './show';
import { ShowSubscriptionManager } from './showSubscriptionManager';
import { Episode } from './episode';
import { ISite } from './iSite';

const LOCK_NAME = 'running';

@singleton()
export class SubscriptionDownloader {
  private readonly lock: AsyncLock = new AsyncLock();

  private readonly sites: Record<string, ISite>;

  public constructor(
    @injectAll('ISite') sites: ISite[],
    private readonly showSubscriptionManager: ShowSubscriptionManager
  ) {
    this.sites = _.keyBy(sites, 'name');
  }

  private async getNewEpisodes(showSubscription: ShowSubscription): Promise<Episode[]> {
    const season = new Show(showSubscription.basePath, showSubscription.showName).getSeason(
      showSubscription.seasonNumber,
      showSubscription.offset ?? 0
    );

    const existingEpisodes = await season.getAllExistingEpisodes();
    const site = this.sites[showSubscription.siteName];
    const episodes = await site.getEpisodesList(season, showSubscription.url);
    return episodes.filter((episode) => !existingEpisodes.includes(episode.episodeNumber));
  }

  private static async DownloadNewEpisode(episode: Episode): Promise<void> {
    await episode.season.createDirectory();
    await episode.site.downloadEpisode(episode);
  }

  public run() {
    return this.lock.acquire(LOCK_NAME, async () => {
      console.log('started download run');
      const showsSubscription = await this.showSubscriptionManager.loadShowsSubscriptions();
      const episodes = _.chain(
        await Promise.all(showsSubscription.map<Promise<Episode[]>>(this.getNewEpisodes.bind(this)))
      )
        .flatten()
        .orderBy(
          [
            (episode) => episode.season.show.basePath,
            (episode) => episode.season.show.name,
            (episode) => episode.season.seasonNumber,
            (episode) => episode.episodeNumber,
          ],
          'asc'
        )
        .value();

      await Promise.all(episodes.map(SubscriptionDownloader.DownloadNewEpisode.bind(this)));
      console.log('finished download run');
    });
  }
}
