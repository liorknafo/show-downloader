import puppeteer, { Browser, Page } from 'puppeteer';
import { Semaphore } from 'await-semaphore';

export class BrowserManager {
  private semaphore: Semaphore;

  constructor(readonly maxBrowserCount: number) {
    this.semaphore = new Semaphore(maxBrowserCount);
  }

  public useBrowser<T>(task: (browser: Browser) => Promise<T> | T): Promise<T> {
    return this.semaphore.use(async () => {
      const browser = await puppeteer.launch();
      try {
        return await task(browser);
      } finally {
        await browser.close();
      }
    });
  }

  public usePage<T>(task: (page: Page) => Promise<T> | T): Promise<T> {
    return this.useBrowser(async (browser) => {
      const page = (await browser.pages())[0];
      return task(page);
    });
  }
}
