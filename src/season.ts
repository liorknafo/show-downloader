/* eslint-disable max-classes-per-file */
import { existsSync } from 'fs';
import { mkdir, readdir } from 'fs/promises';
import path from 'path';
import { ISite } from './iSite';
import { Episode } from './episode';
import type { Show } from './show';

const EPISODE_NUMBER_REGEX = /S(\d+)E(\d+)/;

export class Season {
  constructor(readonly show: Show, readonly seasonNumber: number, readonly offset: number = 0) {}

  public async createDirectory() {
    if (!existsSync(this.seasonPath)) await mkdir(this.seasonPath, { recursive: true });
  }

  public getEpisode(episodeNumber: number, site: ISite, siteEpisodeData: any) {
    return new Episode(this, this.offset + episodeNumber, site, siteEpisodeData);
  }

  public get seasonPath(): string {
    return path.join(
      this.show.basePath,
      this.show.name,
      `Season ${this.seasonNumber.toLocaleString(undefined, { minimumIntegerDigits: 2 })}`
    );
  }

  private static parseEpisodeNameToNumber(name: string): number {
    return parseInt(EPISODE_NUMBER_REGEX.exec(name)?.[2] ?? '0', 10);
  }

  public async getAllExistingEpisodes(): Promise<number[]> {
    if (!existsSync(this.seasonPath)) return [];
    const episodesNames = await readdir(this.seasonPath);
    return episodesNames.map(Season.parseEpisodeNameToNumber);
  }
}
