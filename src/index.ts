import 'reflect-metadata';
import { container } from 'tsyringe';
import cron from 'node-cron';
import './downloaders/gogoanimeDownloader';
import { ShowSubscriptionManager } from './showSubscriptionManager';
import { SubscriptionDownloader } from './subscriptionDownloader';
import { BrowserManager } from './browserManager';
import { WebApp } from './webApp';

const env = process.env.NODE_ENV || 'development';

container.registerInstance('port', env === 'development' ? 8050 : 80);
container.registerInstance('configFile', '/config/subscription_list.json');
container.registerInstance(BrowserManager, new BrowserManager(3));

container.resolve(ShowSubscriptionManager).on('change', () => container.resolve(SubscriptionDownloader).run());
container.resolve(WebApp).listen();
cron.schedule('0 0 * * *', () => container.resolve(SubscriptionDownloader).run());

container.resolve(SubscriptionDownloader).run();
