import express, { Express } from 'express';
import { inject, singleton } from 'tsyringe';
import morgan from 'morgan';
import { ShowSubscriptionManager } from './showSubscriptionManager';
import { SubscriptionDownloader } from './subscriptionDownloader';

@singleton()
export class WebApp {
  app: Express;

  constructor(
    @inject('port') readonly port: number,
    private readonly showSubscriptionManager: ShowSubscriptionManager,
    private readonly subscriptionDownloader: SubscriptionDownloader
  ) {
    this.app = express();

    this.app.use(express.json());
    this.app.use(morgan('short'));

    this.app.post('/show', (req, res) =>
      this.showSubscriptionManager.change((subscriptions) => [...subscriptions, req.body]).then(() => res.send())
    );

    this.app.delete('/show', (req, res) =>
      this.showSubscriptionManager
        .change((subscriptions) => subscriptions.filter((item) => item.url !== req.body.url))
        .then(() => res.send())
    );

    this.app.get('/show', async (_, res) => {
      const shows = await this.showSubscriptionManager.loadShowsSubscriptions();
      res.send(shows);
    });

    this.app.get('/download', async (_, res) => {
      await this.subscriptionDownloader.run();
      res.send();
    });
  }

  public listen() {
    this.app.listen(this.port, () => console.log(`server started at http://localhost:${this.port}`));
  }
}
