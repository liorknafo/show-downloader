import { Season } from './season';

export class Show {
  constructor(readonly basePath: string, readonly name: string) {}

  public getSeason(seasonNumber: number, offset: number = 0): Season {
    return new Season(this, seasonNumber, offset);
  }
}
