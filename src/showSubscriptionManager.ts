import AsyncLock from 'async-lock';
import { existsSync } from 'fs';
import { mkdir, readFile, writeFile } from 'fs/promises';
import path from 'path';
import { EventEmitter } from 'events';
import { inject, singleton } from 'tsyringe';
import type { ShowSubscription } from './showSubscription';

const LOCK_NAME = 'file';

@singleton()
export class ShowSubscriptionManager extends EventEmitter {
  private readonly lock = new AsyncLock();

  constructor(@inject("configFile") readonly filePath: string) {
    super();
  }

  public async loadShowsSubscriptions(): Promise<ShowSubscription[]> {
    try {
      return JSON.parse((await readFile(this.filePath)).toString('utf-8')) as ShowSubscription[];
    } catch (e: any) {
      if (e?.code !== 'ENOENT') throw e;
      return [];
    }
  }

  public acquire_lock<T>(callback: () => T | PromiseLike<T>): Promise<T> {
    return this.lock.acquire(LOCK_NAME, callback);
  }

  public change(
    change_callback: (subscriptions: ShowSubscription[]) => PromiseLike<ShowSubscription[]> | ShowSubscription[]
  ): Promise<void> {
    return this.acquire_lock(async () => {
      await this.writeShowsSubscriptions(await change_callback(await this.loadShowsSubscriptions()));
    });
  }

  public async writeShowsSubscriptions(showsSubscriptions: ShowSubscription[]): Promise<void> {
    if (!existsSync(path.dirname(this.filePath))) await mkdir(path.dirname(this.filePath));
    await writeFile(this.filePath, JSON.stringify(showsSubscriptions));
    this.emit('change', showsSubscriptions);
  }
}
