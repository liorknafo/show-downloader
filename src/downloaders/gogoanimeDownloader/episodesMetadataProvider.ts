import { injectable } from 'tsyringe';
import _ from 'lodash';
import { ElementHandle, Page } from 'puppeteer';
import { GogoanimeEpisode } from './gogoanimeEpisode';
import { BrowserManager } from '../../browserManager';

interface GogoanimeEpisodeSection {
  startEpisode: number;
  endEpisode: number;
  element: ElementHandle;
}

@injectable()
export class EpisodesMetadataProvider {
  public constructor(private readonly browserManager: BrowserManager) {}

  private static async clickEpisodeSectionButton(episodeSection: GogoanimeEpisodeSection, page: Page): Promise<void> {
    await Promise.all([
      page.waitForXPath(`//ul[@id="episode_related"]/li/a/div[.="EP ${episodeSection.endEpisode}"]`),
      page.waitForSelector('ul#episode_page li a.disabled', { hidden: true }),
      episodeSection.element.click(),
    ]);
  }

  private static async getEpisodeSections(page: Page): Promise<GogoanimeEpisodeSection[]> {
    const elements = await page.$$('ul#episode_page li a');
    return Promise.all(
      elements.map<Promise<GogoanimeEpisodeSection>>(async (element) => ({
        element,
        startEpisode: await element.evaluate((e) => parseInt(e.getAttribute('ep_start') ?? '0', 10)),
        endEpisode: await element.evaluate((e) => parseInt(e.getAttribute('ep_end') ?? '0', 10)),
      }))
    );
  }

  private static getCurrentlyShownEpisodes(page: Page): Promise<GogoanimeEpisode[]> {
    return page.$$eval('ul#episode_related li a', (nodes) =>
      (nodes as HTMLAnchorElement[]).map((node: HTMLAnchorElement) => ({
        number: parseInt(node.getElementsByTagName('div').item(0)?.innerText?.match(/\d+/)?.[0] ?? '0', 10),
        url: node.href,
      }))
    );
  }

  public getEpisodesList(url: string): Promise<GogoanimeEpisode[]> {
    return this.browserManager.usePage(async (page) => {
      await page.goto(url, { waitUntil: 'domcontentloaded' });
      const sections = await EpisodesMetadataProvider.getEpisodeSections(page);
      const epiosdes: GogoanimeEpisode[] = [];
      for (const section of sections) {
        /* eslint-disable no-await-in-loop */
        await EpisodesMetadataProvider.clickEpisodeSectionButton(section, page);
        const newEpisodes = await EpisodesMetadataProvider.getCurrentlyShownEpisodes(page);
        epiosdes.push(...newEpisodes);
      }
      return epiosdes;
    });
  }
}
