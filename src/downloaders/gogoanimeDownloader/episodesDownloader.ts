import { injectable } from 'tsyringe';
import { readdir } from 'fs/promises';
import path from 'path';
import { ElementHandle, Page } from 'puppeteer';
import _ from 'lodash';
import { retryAsync, isTooManyTries } from 'ts-retry';
import { DownloadFail } from '../../exceptions';
import { move, useTempDir, waitForFileChange } from '../../utils';
import { BrowserManager } from '../../browserManager';

const TEMP_DIR_NAME_START = 'gogoanime-';

const OPTION_PRIORITY = ['1080', '720', '480', '360'] as const;

@injectable()
export class EpisodesDownloader {
  public constructor(private readonly browserManager: BrowserManager) {}

  private static setDestDownloadLocation(page: Page, downloadDir: string): Promise<void> {
    // eslint-disable-next-line no-underscore-dangle
    return (page as any)._client.send('Page.setDownloadBehavior', { behavior: 'allow', downloadPath: downloadDir });
  }

  private static async clickPopupButton(page: Page, selector: string): Promise<Page> {
    await page.waitForSelector(selector);
    const [newPage] = await Promise.all([
      // eslint-disable-next-line no-promise-executor-return
      new Promise<Page>((resolve) => page.once('popup', resolve)),
      page.click(selector),
    ]);
    return newPage;
  }

  private static async download(downloadButton: ElementHandle, downloadDir: string): Promise<string> {
    const [file] = await Promise.all([this.waitForFileDownload(downloadDir), downloadButton.click()]);
    return file;
  }

  private static async getBestAvailableButton(
    page: Page,
    selector: string,
    priorityList: readonly string[]
  ): Promise<ElementHandle> {
    await page.waitForSelector(selector);
    const buttonsElements = await page.$$(selector);
    const buttons = await Promise.all(
      buttonsElements.map<Promise<{ name: string; item: ElementHandle }>>(async (item) => ({
        name: await item.evaluate((node) => (node as HTMLAnchorElement).innerText),
        item,
      }))
    );

    return _.chain(buttons)
      .filter((link) =>
        _.chain(priorityList)
          .filter((resolution) => link.name.includes(resolution))
          .some()
          .value()
      )
      .orderBy((link) => this.getPriority(link.name, priorityList))
      .value()[0].item as ElementHandle;
  }

  private static async waitForStartFileDownload(downloadDir: string): Promise<void> {
    try {
      if (
        !(await retryAsync(() => this.isDownloading(downloadDir), {
          maxTry: 30,
          delay: 1000,
          until: (i) => i,
        }))
      )
        console.log('not OK');
    } catch (e) {
      if (isTooManyTries(e)) throw new DownloadFail();
      throw e;
    }
  }

  // eslint-disable-next-line consistent-return
  private static async waitForfinishFileDownload(downloadDir: string): Promise<string> {
    // eslint-disable-next-line no-await-in-loop
    while (await this.isDownloading(downloadDir)) await waitForFileChange(downloadDir, 60000);
    return path.join(downloadDir, (await readdir(downloadDir))[0]);
  }

  private static async waitForFileDownload(downloadDir: string): Promise<string> {
    await this.waitForStartFileDownload(downloadDir);
    return this.waitForfinishFileDownload(downloadDir);
  }

  private static async isDownloading(downloadDir: string): Promise<boolean> {
    try {
      const files = await readdir(downloadDir);
      return _.chain(files)
        .some((item) => item.endsWith('.crdownload'))
        .value();
    } catch (e: any) {
      if (e.code === 'ENOENT') return false;
      throw e;
    }
  }

  private static getPriority(label: string, priorityList: readonly string[]): number {
    return _.chain(priorityList)
      .map((val, index) => [index, val] as const)
      .filter((resolution) => label.includes(resolution[1]))
      .map((val) => val[0])
      .min()
      .value();
  }

  public downloadEpisode(url: string, distPath: string): Promise<void> {
    try {
      return retryAsync(
        () =>
          this.browserManager.usePage((gogoanimePage) =>
            useTempDir(TEMP_DIR_NAME_START, async (downloadDir) => {
              await gogoanimePage.goto(url, { waitUntil: 'domcontentloaded' });
              const downloadPage = await EpisodesDownloader.clickPopupButton(gogoanimePage, 'li.dowloads a');
              downloadPage.on('popup', (page) => page.close());
              await EpisodesDownloader.setDestDownloadLocation(downloadPage, downloadDir);
              const downloadedFile = await retryAsync(
                async () => {
                  const downloadButton = await EpisodesDownloader.getBestAvailableButton(
                    downloadPage,
                    'div.dowload a',
                    OPTION_PRIORITY
                  );
                  return EpisodesDownloader.download(downloadButton, downloadDir);
                },
                { delay: 0, maxTry: 3 }
              );
              await move(downloadedFile, `${distPath}${path.extname(downloadedFile)}`);
            })
          ),
        { maxTry: 3, delay: 0 }
      );
    } catch (e) {
      if (isTooManyTries(e)) throw new DownloadFail();
      throw e;
    }
  }
}
