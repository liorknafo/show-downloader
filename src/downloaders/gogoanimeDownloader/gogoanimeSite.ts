import { injectable, registry } from 'tsyringe';
import { ISite } from '../../iSite';
import { EpisodesDownloader } from './episodesDownloader';
import { EpisodesMetadataProvider } from './episodesMetadataProvider';
import type { Season } from '../../season';
import { Episode } from '../../episode';

@injectable()
@registry([{ token: 'ISite', useClass: GogoanimeSite }])
class GogoanimeSite implements ISite {
  public constructor(
    readonly episodesDownloader: EpisodesDownloader,
    readonly episodesMetadataProvider: EpisodesMetadataProvider
  ) {}

  // eslint-disable-next-line class-methods-use-this
  public get name(): string {
    return 'gogoanime';
  }

  public async getEpisodesList(season: Season, url: string): Promise<Episode[]> {
    return (await this.episodesMetadataProvider.getEpisodesList(url)).map((episode) =>
      season.getEpisode(episode.number, this, episode.url)
    );
  }

  public downloadEpisode(episode: Episode): Promise<void> {
    return this.episodesDownloader.downloadEpisode(episode.siteEpisodeData, episode.episodePath);
  }
}
export default GogoanimeSite
